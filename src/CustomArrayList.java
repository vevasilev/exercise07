public class CustomArrayList<T> {

    private static final int DEFAULT_CAPACITY = 10;

    private T[] array;
    private int capacity;
    private int size;

    public CustomArrayList() {
        this.capacity = DEFAULT_CAPACITY;
        this.size = 0;
        array = (T[]) new Object[capacity];
    }

    public void add(T value) {
        add(size, value);
    }

    public void add(int index, T value) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + " outside the bounds of the array");
        } else if (size == capacity) {
            T[] previousArray = array;
            capacity = previousArray.length + (previousArray.length / 2) + 1;
            array = (T[]) new Object[capacity];
            System.arraycopy(previousArray, 0, array, 0, index);
            System.arraycopy(previousArray, index, array, index + 1, size - index);
            array[index] = value;
            size++;
        } else {
            System.arraycopy(array, index, array, index + 1, size - index);
            array[index] = value;
            size++;
        }
    }

    public T remove(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + " outside the bounds of the array");
        } else {
            var element = array[index];
            System.arraycopy(array, index + 1, array, index, size - index - 1);
            array[--size] = null;
            return element;
        }
    }

    public boolean remove(Object element) {
        for (int index = 0; index < size; index++) {
            if (element.equals(array[index])) {
                return remove(index) == element;
            }
        }
        return false;
    }

    public boolean contains(Object element) {
        for (Object value : array) {
            if (element.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T get(int index) {
        return array[index];
    }

    public void clear() {
        for (int index = size - 1; index >= 0; index--) {
            array[index] = null;
            size--;
        }
        if (capacity != DEFAULT_CAPACITY) {
            capacity = DEFAULT_CAPACITY;
            array = (T[]) new Object[capacity];
        }
    }

    @Override
    public String toString() {
        if (size != 0) {
            StringBuilder result = new StringBuilder("[");
            for (int index = 0; index < size - 1; index++) {
                result.append(array[index]).append(", ");
            }
            result.append(array[size - 1]).append("]");
            return result.toString();
        }
        return "[]";
    }
}