public class Exercise06 {
    public static void main(String[] args) {
        System.out.println("Create and fill in a custom ArrayList:");
        CustomArrayList<Integer> array = new CustomArrayList<>();
        array.add(2);
        array.add(3);
        array.add(5);
        array.add(6);
        array.add(7);
        System.out.println(array);
        System.out.println();

        System.out.println("Let's add an element by index at the beginning and in the middle: ");
        array.add(0, 1);
        array.add(3, 4);
        System.out.println(array);
        System.out.println();

        System.out.println("Delete the elements at the beginning and at the end: ");
        array.remove(0);
        array.remove(Integer.valueOf(7));
        System.out.println(array);
        System.out.println();

        var value = 20;
        System.out.println("Let's check the presence of the element " + value + ": " + array.contains(value));
        System.out.println("Let's add it and check its availability again.");
        array.add(value);
        System.out.println(array);
        System.out.println("Let's check the presence of the element " + value + ": " + array.contains(value));
        System.out.println();

        System.out.println(array);
        System.out.println("Custom ArrayList size: " + array.size());
        array.add(0, 1);
        System.out.println(array);
        System.out.println("Custom ArrayList size: " + array.size());
        System.out.println();

        System.out.println("Custom ArrayList is empty? " + array.isEmpty());
        System.out.println();

        System.out.println("Element with index 0: " + array.get(0));
        System.out.println("Element with index 1: " + array.get(1));
        System.out.println();

        System.out.println("Let's clear the custom ArrayList");
        array.clear();
        System.out.println(array);
    }
}
